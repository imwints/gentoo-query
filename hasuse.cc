#include <fstream>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <string_view>

std::string file_to_string(const std::filesystem::path& path) {
  std::ifstream stream { path };
  return std::string { std::istream_iterator<char>(stream), std::istream_iterator<char>() };
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    return 1;
  }

  std::string_view flag_to_find { argv[1] };

  const std::filesystem::path root{ "/" };
  const std::filesystem::path pkgdb { "var/db/pkg" };
  for (const auto& category : std::filesystem::directory_iterator(root / pkgdb)) {
    // std::cout << category.path().string() << std::endl;
    for (const auto& pkg : std::filesystem::directory_iterator(category)) {
      // std::cout << '\t' << pkg.path().string() << std::endl;

      std::string use = file_to_string(pkg.path() / "USE");
      std::ifstream iuse { pkg.path() / "USE" };

      for (std::string use_flag; iuse >> use_flag;) {
        if (use_flag.size() > 0 && use_flag[0] == '+') {
          use_flag.erase(0, 1);
        }
        if (flag_to_find == use_flag && use.find(use_flag) != std::string::npos) {
          // SUCCESS!
          std::cout << (category.path().filename() / pkg.path().filename()).string() << '\n';
          break;
        }
      }
    }
  }
}
